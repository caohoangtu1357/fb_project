/**
 * 
 * api reactions
 * 
 * https://graph.facebook.com/v11.0?ids=2856129031323093&fields=reactions.type(LIKE).limit(0).summary(total_count).as(reactions_like),reactions.type(LOVE).limit(0).summary(total_count).as(reactions_love),reactions.type(WOW).limit(0).summary(total_count).as(reactions_wow),reactions.type(HAHA).limit(0).summary(total_count).as(reactions_haha),reactions.type(SAD).limit(0).summary(total_count).as(reactions_sad),reactions.type(ANGRY).limit(0).summary(total_count).as(reactions_angry),reactions.type(THANKFUL).limit(0).summary(total_count).as(reactions_thankful),reactions.type(PRIDE).limit(0).summary(total_count).as(reactions_pride),reactions.type(CARE).limit(0).summary(total_count).as(reactions_care)&access_token=EAAAAZAw4FxQIBAPaQZCAqZC4ZAdOPuZBZBnc6cQSe9VZAEMJDNKbZAsXqo6p2HxH0RiJ1ZBc5821INTER4iQ8SrdTsPP2OxXxpkdJkOinZAZCuQJtXh3WN3fRyy2DyaEZB16IUdDDeZBcWq4lRaITPP6tWsZA4dZBydDt7uJ1Hll7aa3UPQbEIj6JeZAahNL
 * 
 */

/**
 * 
 * api comments
 * https://graph.facebook.com/v11.0?ids=2940575266211802&fields=comments.summary(true).limit(0)&access_token=EAAAAZAw4FxQIBAPaQZCAqZC4ZAdOPuZBZBnc6cQSe9VZAEMJDNKbZAsXqo6p2HxH0RiJ1ZBc5821INTER4iQ8SrdTsPP2OxXxpkdJkOinZAZCuQJtXh3WN3fRyy2DyaEZB16IUdDDeZBcWq4lRaITPP6tWsZA4dZBydDt7uJ1Hll7aa3UPQbEIj6JeZAahNL
 * 
 */


/**
 * video
 * https://graph.facebook.com/v7.0/249178480408187/video_insights?metric=total_video_views,total_video_views_unique&access_token=EAAAAZAw4FxQIBAPaQZCAqZC4ZAdOPuZBZBnc6cQSe9VZAEMJDNKbZAsXqo6p2HxH0RiJ1ZBc5821INTER4iQ8SrdTsPP2OxXxpkdJkOinZAZCuQJtXh3WN3fRyy2DyaEZB16IUdDDeZBcWq4lRaITPP6tWsZA4dZBydDt7uJ1Hll7aa3UPQbEIj6JeZAahNL
 * 
 */

