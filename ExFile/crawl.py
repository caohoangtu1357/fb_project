import sys
import os
from selenium import webdriver
import urllib.request
import json
import urllib.request
from bs4 import BeautifulSoup
import time
import re

class Crawler:
    
    driver = None
    driverPath = "/usr/local/bin/chromedriver"
    token = "EAAAAZAw4FxQIBAEtiiPbXY4RUrl6phOQX0EpvCaLJbVzKTusoi6BAvPy6yMvx3fAEZB1KBUFp5CUPZC4CsYZCqU4HkIeC766tHR3VJ5W32hZAd2lspF2V8XPSA0f9fJDRFpNkj56YXH0OBqzSR8ih4dY20Wh0Xot0t5y9fzVFRQZDZD"
    cookie = "sb=kcBCYfHVknIF_bl2oVU8a9Cw; datr=kcBCYajwpPh39C7BzI4ORfDY; _fbp=fb.1.1631764858151.10195567; wd=1848x949; locale=vi_VN; c_user=100007780712575; xs=27:JBHuCF-YTKycrw:2:1631785556:-1:6229; fr=1PpxbNRTKu1ZfvaPy.AWWrOEVx6-f4fOSAvaTacRw6DN4.BhQwL6.57.AAA.0.0.BhQxJU.AWXntqsKgM0; spin=r.1004405196_b.trunk_t.1631785560_s.1_v.2_"
    groupId= None
    userId = None
    postId = None

    def __init__(self):
        options = webdriver.chrome.options.Options()
        options.add_argument("--disable-infobars")
        options.add_argument("--disable-extensions")
        options.add_experimental_option("prefs", { 
            "profile.default_content_setting_values.notifications": 1 
        })

        options.headless = False
        self.driver = webdriver.Chrome(options=options, executable_path=self.driverPath)

        self.driver.get("https://www.facebook.com")
        time.sleep(5)
        self.driver.execute_script('function setCookie(t) {var list = t.split("; ");console.log(list);for (var i = list.length - 1; i >= 0; i--) {var cname = list[i].split("=")[0];var cvalue = list[i].split("=")[1];var d = new Date();d.setTime(d.getTime() + (7 * 24 * 60 * 60 * 1000));var expires = ";domain=.facebook.com;expires=" + d.toUTCString();console.log(expires);document.cookie = cname + "=" + cvalue + "; " + expires;}}setCookie("'+self.cookie+'");')

    def getGroupId(self):
        src = self.driver.page_source
        ids = re.findall('group_id":"[0-9]+"', src)
        if ids:
            group_id = re.findall('[0-9]+',ids[0])
            return group_id
        return None

    def getComments(self):
        apiPost = "https://graph.facebook.com/{}/comments?access_token={}&summary=1&filter=toplevel&limit=1000".format(self.postId,self.token)
        response = urllib.request.urlopen(apiPost).read()
        jsonResponse = json.loads(response.decode('utf-8'))
        return jsonResponse['data']

    def getUserInfo(self,uid):
        apiUserInfo = "https://graph.facebook.com/{}?access_token={}&summary=1&filter=toplevel&limit=10".format(self.token,uid)
        response = urllib.request.urlopen(apiUserInfo).read()
        jsonResponse = json.loads(response.decode('utf-8'))
        return jsonResponse
        
    def getCurrentUserInfo(self):
        apiMe = "https://graph.facebook.com/me?access_token={}".format(self.token)
        response = urllib.request.urlopen(apiMe).read()
        me = json.loads(response.decode('utf-8'))
        return me
    

    def extractCommentUid(self, request, *args, **kwargs):
    
        # extract comment data 

        self.driver.get("https://www.facebook.com/{}".format(self.postId))
        time.sleep(3)
        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        src = self.driver.page_source
        soup = BeautifulSoup(src, 'html.parser')
        for comment in self.getComments():
            aTags = soup.find_all("a", href=re.compile(comment['id']))
            uid=False
            for aTag in aTags:
                uid = (re.findall("100[0-9]{12}", aTag.parent.parent.parent.parent.find_all("a",href=re.compile("/user/"))[0]['href']))
            if uid:
                print(uid,"------>",comment['message'])

        # extract reaction data

        self.driver.get("https://www.facebook.com/{}".format(self.postId))
        time.sleep(3)
        src = self.driver.page_source
        soup = BeautifulSoup(src, 'html.parser')

        time.sleep(2)
        self.driver.execute_script(open("./test.js").read())
        time.sleep(10)
        src = self.driver.page_source
        soup = BeautifulSoup(src, 'html.parser')
        
        element = soup.find_all("a",href=re.compile(self.userId))
        boxAlert = (element[0].parent.parent.parent.parent.parent.parent.parent)

        for i in range(0,10):
            self.driver.execute_script("var elements = document.querySelectorAll('."+'.'.join(boxAlert.get("class"))+"');for(let i=0;i<elements.length;i++){elements[i].scrollTop+=1000;}")
            time.sleep(3)
            src = self.driver.page_source
            soup = BeautifulSoup(src, 'html.parser')
            element = soup.find_all("a",href=re.compile(self.userId))
            boxAlert = (element[0].parent.parent.parent.parent.parent.parent.parent)
            aTags = boxAlert.find_all("a",href=re.compile("100[0-9]{12}"))
            for aTag in aTags:
                if aTag.text:
                    uid = (re.findall("100[0-9]{12}", aTag.get('href')))
                    print(aTag.text,'-------->',uid)

    def getFeeds(self):
        apiFeed = "https://graph.facebook.com/{}/feed?access_token={}&summary=1&filter=toplevel&limit=3".format(self.groupId,self.token)
        response = urllib.request.urlopen(apiFeed).read()
        feeds = json.loads(response.decode('utf-8'))
        user = self.getCurrentUserInfo()
        crawled=[]
        for feed in feeds['data']:
            if feed not in crawled: 
                self.extractCommentUid(feed['id'],user['id'])
                crawled.append(feed['id'])