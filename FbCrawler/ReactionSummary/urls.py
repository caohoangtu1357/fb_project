from django.urls import path
from ReactionSummary import views
urlpatterns = [
     path('fb-crawler/sumary-reaction/', views.Summary.as_view()),
]