from django.http import JsonResponse

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
import urllib.request
import json

class Summary(APIView):
    
    def callSummaryApi(self,postId,accessToken):

        apiSummaryReaction = "https://graph.facebook.com/v11.0?ids={}&fields=reactions.type(LIKE).limit(0).summary(total_count).as(reactions_like),reactions.type(LOVE).limit(0).summary(total_count).as(reactions_love),reactions.type(WOW).limit(0).summary(total_count).as(reactions_wow),reactions.type(HAHA).limit(0).summary(total_count).as(reactions_haha),reactions.type(SAD).limit(0).summary(total_count).as(reactions_sad),reactions.type(ANGRY).limit(0).summary(total_count).as(reactions_angry),reactions.type(THANKFUL).limit(0).summary(total_count).as(reactions_thankful),reactions.type(PRIDE).limit(0).summary(total_count).as(reactions_pride),reactions.type(CARE).limit(0).summary(total_count).as(reactions_care)&access_token={}".format(postId,accessToken)
        apiSummaryComment = "https://graph.facebook.com/{}/comments?summary=1&filter=toplevel&access_token={}".format(postId,accessToken)
        
        try:
            summaryComments = urllib.request.urlopen(apiSummaryComment).read()
            summaryReaction = urllib.request.urlopen(apiSummaryReaction).read()
            summaryComments = json.loads(summaryComments.decode('utf-8'))
            summaryReaction = json.loads(summaryReaction.decode('utf-8'))
        except Exception as error:
            raise error
        
        totalComment = summaryComments["summary"]["total_count"]
        totalReaction = summaryReaction[postId]

        return {"data":{"summary_comment":totalComment,"summary_reaction":totalReaction}}

    def post(self,request, *args, **kwargs):
        requestData = request.data
        try:
            summary = self.callSummaryApi(requestData["post_id"],requestData["access_token"])
        except:
            return Response(data={"msg":"Invalid post_id"},status=status.HTTP_400_BAD_REQUEST)
        
        return Response(data=summary,status=status.HTTP_200_OK)

    