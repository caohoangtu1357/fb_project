from django.apps import AppConfig


class CrawlreactionConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'CrawlReaction'
