from rest_framework import serializers

class CrawlReaction(serializers.Serializer):
    group_url = serializers.CharField(required=True)
    fb_cookie = serializers.CharField(required=True)
    num_feed = serializers.IntegerField(required=True)