from django.http import JsonResponse

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from CrawlReaction import serializers

from selenium import webdriver
import urllib.request
import json
import urllib.request
from bs4 import BeautifulSoup
import time
import re
import os
import sys
from webdriver_manager.chrome import ChromeDriverManager


class CrawlData(APIView):
    
    driver = None
    driverPath = os.path.join(sys.path[0], "CrawlReaction/driver/chromedriver")
    token = None
    cookie = None
    groupId= None
    user = None
    postId = None
    option = None

    def __init__(self):
        options = webdriver.chrome.options.Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        options.add_argument("--disable-infobars")
        options.add_argument("--disable-extensions")
        options.add_experimental_option("prefs", { 
            "profile.default_content_setting_values.notifications": 1 
        })
        options.headless = True
        self.options=options
        
    def startDriver(self):
        self.driver = webdriver.Chrome(options=self.options, executable_path=ChromeDriverManager().install())
        self.driver.get("https://www.facebook.com")

    def loginFB(self):
        self.driver.execute_script('function setCookie(t) {var list = t.split("; ");console.log(list);for (var i = list.length - 1; i >= 0; i--) {var cname = list[i].split("=")[0];var cvalue = list[i].split("=")[1];var d = new Date();d.setTime(d.getTime() + (7 * 24 * 60 * 60 * 1000));var expires = ";domain=.facebook.com;expires=" + d.toUTCString();console.log(expires);document.cookie = cname + "=" + cvalue + "; " + expires;}}setCookie("'+self.cookie+'");')

    def getFbToken(self):
        self.driver.get("https://m.facebook.com/composer/ocelot/async_loader/?publisher=feed")
        src = self.driver.page_source
        tokens = re.findall('EAAA[a-zA-Z0-9]+', src)
        if tokens:
            return tokens[0]
        return None

    def getGroupId(self,url):
        self.driver.get(url)
        src = self.driver.page_source
        ids = re.findall('group_id":"[0-9]+"', src)
        if ids:
            group_id = re.findall('[0-9]+',ids[0])
            if(group_id):
                return group_id[0]
        return None

    def getComments(self,post_id):
        apiPost = "https://graph.facebook.com/{}/comments?access_token={}&summary=1&filter=toplevel&limit=1000".format(post_id,self.token)
        response = urllib.request.urlopen(apiPost).read()
        jsonResponse = json.loads(response.decode('utf-8'))
        return jsonResponse['data']

    def getUserInfo(self,uid):
        try:
            apiUserInfo = "https://graph.facebook.com/{}?access_token={}".format(uid,self.token)
        except Exception as error:
            pass
            return []
        response = urllib.request.urlopen(apiUserInfo).read()
        jsonResponse = json.loads(response.decode('utf-8'))
        return jsonResponse
        
    def getCurrentUserInfo(self):
        apiMe = "https://graph.facebook.com/me?access_token={}".format(self.token)
        response = urllib.request.urlopen(apiMe).read()
        me = json.loads(response.decode('utf-8'))
        return me

    def post(self,request, *args, **kwargs):

        validation = serializers.CrawlReaction(data=request.data)
        if not validation.is_valid():
            return Response(data={"msg":validation.errors},status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        self.startDriver()
        data = []
        requestData = request.data
        self.cookie = requestData["fb_cookie"]
        self.loginFB()
        self.token = self.getFbToken()

        if not self.token:
            self.driver.close()
            return Response(data={"msg":"cookie invalid"},status=status.HTTP_400_BAD_REQUEST)

        self.groupId = self.getGroupId(requestData['group_url'])
        if not self.groupId:
            self.driver.close()
            return Response(data={"msg":"id group not found"},status=status.HTTP_404_NOT_FOUND)

        self.user = self.getCurrentUserInfo()
        feeds = self.getFeeds(requestData['num_feed'])
        crawled=[]

        for feed in feeds['data']:
            if feed not in crawled: 
                data.append(self.extractCommentUid(feed['id']))
                crawled.append(feed['id'])

        self.driver.close()
        return Response(data=data,status=status.HTTP_200_OK)

    def extractCommentUid(self,postId):
        # extract comment data

        data = {
            "comments":[],
            "reactions":[]
        }

        self.driver.get("https://www.facebook.com/{}".format(postId))
        time.sleep(3)
        self.driver.execute_script(open(os.path.join(sys.path[0], "CrawlReaction/scripts/scrollComment.js")).read())
        src = self.driver.page_source
        soup = BeautifulSoup(src, 'html.parser')
        for comment in self.getComments(postId):
            aTags = soup.find_all("a", href=re.compile(comment['id']))
            uid=False
            for aTag in aTags:
                uid = (re.findall("100[0-9]{12}", aTag.parent.parent.parent.parent.find_all("a",href=re.compile("/user/"))[0]['href']))
            if uid:
                data["comments"].append({"uid":uid[0],"message":comment['message']})


        # extract reaction data

        self.driver.get("https://www.facebook.com/{}".format(postId))
        time.sleep(3)
        src = self.driver.page_source
        soup = BeautifulSoup(src, 'html.parser')

        time.sleep(2)
        self.driver.execute_script(open(os.path.join(sys.path[0], "CrawlReaction/scripts/scrollReaction.js")).read())
        time.sleep(15)
        src = self.driver.page_source
        soup = BeautifulSoup(src, 'html.parser')
        
        boxAlert = soup.find_all("a",href=re.compile(self.user['id']))[0]

        for loopParent in range(1,14):
            boxAlert = boxAlert.parent
            aTags = boxAlert.find_all("a",href=re.compile("100[0-9]{12}"))
            if len(aTags) >2:
                break
        
        listUid = []
        for i in range(0,6):
            self.driver.execute_script("var elements = document.querySelectorAll('."+'.'.join(boxAlert.get("class"))+"');for(let i=0;i<elements.length;i++){    if('"+' '.join(boxAlert.get("class"))+"' == elements[i].getAttribute('class')){        elements[i].parentNode.scrollTop+=1000;    }}")
            time.sleep(3)
            src = self.driver.page_source
            soup = BeautifulSoup(src, 'html.parser')

            boxAlert = soup.find_all("a",href=re.compile(self.user['id']))[0]
            for loopParent in range(1,14):
                boxAlert = boxAlert.parent
                aTags = boxAlert.find_all("a",href=re.compile("100[0-9]{12}"))
                if len(aTags) >2:
                    break

            aTags = boxAlert.find_all("a",href=re.compile("100[0-9]{12}"))
            for aTag in aTags:
                if aTag.text:
                    uid = re.findall("100[0-9]{12}", aTag.get('href'))
                    if(uid):
                        uid=uid[0]
                        if(uid not in listUid):
                            data["reactions"].append(self.getUserInfo(uid)) 
                            listUid.append(uid)
        return data

    def getFeeds(self,numFeed):
        apiFeed = "https://graph.facebook.com/{}/feed?access_token={}&summary=1&filter=toplevel&limit={}".format(self.groupId,self.token,numFeed)
        response = urllib.request.urlopen(apiFeed).read()
        feeds = json.loads(response.decode('utf-8'))
        return feeds


class CrawlUserProfile(APIView):

    def getUidByUrl(self,url):
        response = urllib.request.urlopen(url).read()
        htmls = re.findall('"userID":"[0-9]{15}"', str(response))
        uid=""
        for html in htmls:
            uids = re.findall('[0-9]+',html)
            if len(uids) > 0:
                uid = uids[0]
        return uid

    def post(self,request, *args, **kwargs):
        requestData = request.data
        accessToken = requestData["access_token"]
        profileUrl = requestData["profile_url"]
        uid = self.getUidByUrl(profileUrl)

        try:
            apiUserInfo = "https://graph.facebook.com/{}?access_token={}".format(uid,accessToken)
            response = urllib.request.urlopen(apiUserInfo).read()
            jsonResponse = json.loads(response.decode('utf-8'))
            return Response(data=jsonResponse,status=status.HTTP_200_OK)

        except Exception as error:
            return Response(data={"msg":"Invalid profile url."},status=status.HTTP_400_BAD_REQUEST)