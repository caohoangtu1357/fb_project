from django.urls import path
from CrawlReaction import views
urlpatterns = [
     path('fb-crawler/crawl-reaction/', views.CrawlData.as_view()),
     path('fb-crawler/crawl-user-profile', views.CrawlUserProfile.as_view())
]